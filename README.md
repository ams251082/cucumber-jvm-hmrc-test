**My Notes** :- This is the usual way I follow to implement any basic framework
I have chosen basic cucumber jvm framework to implement this automation test. Cucumber Gherkin provides a simple way to implement automation test for BDD.
	-	Cucumber is a tool that executes plain-text functional descriptions as automated tests
	-	Tests written in business-facing text
		o	Supports over 40 spoken languages
		o	This test is called Gherkin
	-	Automation written in target language
		o	Java 
	-	Feature file:- Scenarios are organized together into features
		o	Each feature is represented as a plain text file. 
		o	Feature files must have the “.feature” extension
		o	Each  feature can contain many scenarios
	-	Step Definitions :- 
		o	A single Method representing a Step from a Feature file
		o	The Method is annotated with information about the step it represents
		o	Cucumber generates “Code Snippet's” for Step Definition it can’t find
	-	Page Objects :-
		o	Classes to identify objects on a webpage using id, class, css, xpath etc
		o	These page objects will then be called in the step definitions and the relevant actions will be performed on those objects 
Tools used :- Intellij / VSCode

======================================================
The libraries or dependent jars that we will use are

cucumber-java: This library is being developed and is dependent on cucumber-core, which has most of the core api’s
cucumber-testng: cucumber can be run through testng too
cucumber-junit: This is the most popular way to kick off cucumber tests written in java
selenium-java: Selenium jars that are required for our browser based tests.
junit, log4j, apache-poi, xmlapis, xerces: All of these jars will be used over time.
maven-surefire plugin: This plugin is used for kicking off junit, testng and java main classes etc. It just makes life easy

======================================================
Project structure :- 
src/main/java: This will ideally contain source code
src/test/java: This will contain all java code that runs automation
src/test/resources: This will contain our feature files
BlankStepDefs.java: This file contains the step definitions. We will explain below. There can be any number of such files with different names [java classes]
Hooks.java: I have put this class so that we can have @Before and @After hooks for cucumber in one place. This contains the code for pre and post conditions of every scenario run.
RunCukesTest.java: This file contains the logic that will kick off the cucumber scenarios and other cucumber options that might be required.
features: This folder contains all the feature files

======================================================
Setup :-
======================================================
1.	Install JDK
Step 1
Download JDK7 from oracle website downloads page. I am using JDK7, however if you have version 8 , that is fine too.
Step 2
Accept license agreement
Step 3
Select the java version according to your operating system environment. In my case it is 64 bit windows. Download and save it. Double click and run the setup.
Step 5
Follow the regular process of installation of any software on windows and go with defaults. In my case, it got installed to program files\jdk as below
Step 6
Now go ahead and create a environment variable JDK_HOME and point it to the installation directory. After that add %JDK_HOME%/bin to PATH variable
Step 7
Now open a command line and type “java -version”, it should show the version installed.

======================================================
2.	Install and configure Maven
Download maven from apache website
Step 2
Unzip on a local drive. I unzipped to c:\apache…
Step 3
Now set an environment variable M2_HOME=c:\apache… and append %M2_HOME%\bin to PATH variable. See previous section on how we set JDK_HOME. It is similar. Once you do that, open a command line and type “mvn -version”. It should print something like the below [this will complain if it doesn’t find JDK]
Exceptions
If you are in a corporate/enterprise environment, then you might have to do extra steps like.

%M2_HOME%/conf/settings.xml will be specific to your environment. This is so that connect to an internal maven repository and not necessarily maven central
The conf might point to Nexus or like repositories, so you might have to configure that
We are assuming here that you have access to maven central repository and all other dependent sites access.

======================================================
3.	Install IntelliJ Community Edition
Download intelliJ Community edition from here. I downloaded ideaIC-14.0.2.exe on windows machine
Double Click “ideaIC-14.0.2.exe” file and go with all defaults as a regular Windows installable. After that, open the IDE and below is what you would see. Click “Import Project”
Now point it to the location where your project is. In my case, I pointed it to the Class1 folder
Select “Import project from external model” and select “Maven” as below and hit next.
Go with the options as below (default) and click Next
The project is recognized as maven project and click Next
Since this is the first time, intelliJ might not be able to locate your JDK, hence follow the below instructions in the order of 1,2,3 and point to the JDK installed on your machine. Please follow the instructions on how to install JDK and click Next.
Now JDK is identified and the window should look as below. Click Next
Now enter the name of project. I left it as is. You can rename if you would like.
The project is finally imported and some description that I put up for your reference on how intelliJ identifies various files and folders.
Install Cucumber Java Plugin
Go ahead with all defaults and install the cucumber java plugin for IntelliJ. At this point, your intellij is ready to start developing cucumber jvm + any automation library scripts

======================================================
4.	Set up selenium webdriver components
1) Firefox Browser
Firefox browser DOES NOT require any extra Selenium webdriver components

2) Chrome Browser
a) Download chromedriver.exe from here. Get the latest version
b) Unzip chromedriver_win32.zip into C:\SeleniumWebdrivers (assuming you are using windows). The file chromedriver.exe should be present
c) Now add “C:\SeleniumWebdrivers folder to the Windows PATH variable. If you are using Windows 7, go to Control Panel -> Users -> Add/Change environment variables
d) Click ‘New’ under User variables section and do the below
e) Verify that chromedriver.exe can be started successfully
f) Hit CTRL+c and kill the process

3) Internet Explorer
a) Download IEDriverServer.exe from here. Get the latest version
b) Unzip IEDriverServer_Win32_2.44.0.zip [OR 64bit based on your machine] into C:\SeleniumWebdrivers (assuming you are using windows). The file IEDriverServer.exe should be present
c) Now add “C:\SeleniumWebdrivers folder to the Windows PATH variable. If you are using Windows 7, go to Control Panel -> Users -> Add/Change environment variables.
d) Click ‘New’ under User variables section and do the below
Note: Do c) and d) ONLY if you have NOT already done so in chromedriver.exe section above
e) Verify that IEDriverServer.exe can be started successfully
f) Hit CTRL+c and kill the process

========================================================
Download template project and import in to eclipse/intellij
cucumber-jvm and other dependent jars

========================================================
Execution :-
RunCukesTest from step definitions 
OR
Command line Execution

Go to the root folder of the project in command line and type “mvn clean test”. This should pass too
