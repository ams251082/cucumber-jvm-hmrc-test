package step_definitions;

import static org.testng.AssertJUnit.assertEquals;

import org.openqa.selenium.WebDriver;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import helpers.Log;
import org.testng.Reporter;

import pageobjects.GruyereHomePage;
import pageobjects.GruyereSignupPage;
import pageobjects.GruyereSignedInPage;
import pageobjects.NewSnippetPage;
import pageobjects.ExistingSnippetsPage;

import java.util.UUID;


public class AddSnippetsStepDefs{
    public WebDriver driver;
    
    public AddSnippetsStepDefs()
    {
    	driver = Hooks.driver;
    }

    @Then("^New Snippet page is shown$")
    public void validate_new_snippet_page() throws Throwable {
      assertEquals("Gruyere: New Snippet",driver.getTitle());
    }

    @When("^I enter \"([^\"]*)\" in the new snippet textbox$")
    public void enter_new_snippet_text(String text) throws Throwable {
        NewSnippetPage.text_area.sendKeys(text);
    }

    @When("^I click on Submit$")
    public void click_submit_new_snippet() throws Throwable {
        NewSnippetPage.submit_button.click();
    }
    
}