package step_definitions;

import static org.testng.AssertJUnit.assertEquals;
import static org.testng.AssertJUnit.assertFalse;

import org.openqa.selenium.WebDriver;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import helpers.Log;
import org.openqa.selenium.support.PageFactory;

//import pageobjects.GruyereHomePage;
import pageobjects.*;


public class SignupStepDefs{
    public WebDriver driver;
    
    public SignupStepDefs()
    {
    	driver = Hooks.driver;
    }
    
    @When("^I open google-gruyere website$")
    public void open_google_gruyere_website() throws Throwable {
        driver.get("https://google-gruyere.appspot.com/165317495946/");
    }

    @Then("^Gruyere: Home website is launched$")
    public void validate_title_and_URL() throws Throwable {
      assertEquals("Gruyere: Home",driver.getTitle());
      assertEquals("https://google-gruyere.appspot.com/165317495946/",driver.getCurrentUrl());
    }

    @When("^I click signup$")
    public void click_signup() throws Throwable {
        PageFactory.initElements(driver, GruyereHomePage.class);
        PageFactory.initElements(driver, GruyereSignupPage.class);
        PageFactory.initElements(driver, GruyereSignedInPage.class);
        PageFactory.initElements(driver, NewSnippetPage.class);
        PageFactory.initElements(driver, ExistingSnippetsPage.class);
        GruyereHomePage.sign_up.click();
    }
    
    @Then("^Signup for a new account page is shown$")
    public void validate_title_and_URL_Signup() throws Throwable {
      assertEquals("Gruyere: Profile",driver.getTitle());
      assertEquals("https://google-gruyere.appspot.com/165317495946/newaccount.gtl",driver.getCurrentUrl());
    }

    @When("^I signup for a new account$")
    public void signup_random() throws Throwable {

        //Create a 10 digit random number - so that every signup is for a new user
        long random_number = (long) Math.floor(Math.random() * 9000000000L) + 1000000000L;
        Log.info("UserID and password random number is" +random_number);
        
        //Enter user id and password as the above number and signup
        GruyereSignupPage.user_name.sendKeys(Long.toString(random_number));
        GruyereSignupPage.password.sendKeys(Long.toString(random_number));
        GruyereSignupPage.create_account.click();
    
    }

    @When("^I signup for a new account with blank id and password$")
    public void signup_null_id_password() throws Throwable {
        GruyereSignupPage.create_account.click();
    
    }

    @When("^\"([^\"]*)\" message should be displayed$")
    public void display_msg(String message) throws Throwable {
        assertEquals("Account created.",GruyereSignupPage.user_message.getText());
    }

    @When("^error message should be displayed$")
    public void display_msg() throws Throwable {
        assertEquals("Please enter correct username and password",GruyereSignupPage.user_message.getText());
    }
}