package step_definitions;

import static org.testng.AssertJUnit.assertEquals;

import com.siyeh.ipp.chartostring.StringToCharIntention;
import org.openqa.selenium.WebDriver;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import helpers.Log;
import org.openqa.selenium.support.PageFactory;
import org.testng.Reporter;

import pageobjects.GruyereHomePage;
import pageobjects.GruyereSignupPage;
import pageobjects.GruyereSignedInPage;
import pageobjects.NewSnippetPage;
import pageobjects.ExistingSnippetsPage;

import java.util.UUID;


public class SignedInStepDefs{
    public WebDriver driver;
    
    public SignedInStepDefs()
    {
    	driver = Hooks.driver;
    }
    
    @When("^I click on New Snippet link in the header$")
    public void click_header_link() throws Throwable {
          GruyereSignedInPage.new_snippet.click();
    }

    @When("^I click on Home link in the header$")
    public void click_home_header_link() throws Throwable {
        GruyereSignedInPage.home_link.click();
    }
}