package step_definitions;

import static org.testng.AssertJUnit.assertEquals;

import org.openqa.selenium.WebDriver;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import helpers.Log;
import org.testng.Reporter;

import pageobjects.GruyereHomePage;
import pageobjects.GruyereSignupPage;
import pageobjects.GruyereSignedInPage;
import pageobjects.NewSnippetPage;
import pageobjects.ExistingSnippetsPage;

import java.util.UUID;


public class MySnippetsStepDefs{
    public WebDriver driver;
    
    public MySnippetsStepDefs()
    {
    	driver = Hooks.driver;
    }
    
    // Then I should be able to see My Snippets page
    @When("^I should be able to see My Snippets page$")
    public void validate_my_snippet_page() throws Throwable {
      assertEquals("Gruyere: Snippets",driver.getTitle());
    }

    // And the text under All snippets should be "this is a new snippet test"
     @When("^the text under All snippets should be \"([^\"]*)\"$")
    public void validate_my_snippet_text(String text) throws Throwable {
        assertEquals(text,ExistingSnippetsPage.first_snippet_text.getText());
    }
}