package pageobjects;
import helpers.Log;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
public class GruyereSignedInPage extends BaseClass{

    public GruyereSignedInPage(WebDriver driver){
        super(driver);
    }

    @FindBy(how=How.LINK_TEXT, using="New Snippet")
    public static WebElement new_snippet;

    @FindBy(how=How.LINK_TEXT, using="Home")
    public static WebElement home_link;

    @FindBy(how=How.LINK_TEXT, using="My Snippets")
    public static WebElement my_snippets;

    @FindBy(how=How.XPATH, using="//h2[@class='has-refresh']")
    public static WebElement page_title;
}