package pageobjects;
import helpers.Log;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
public class ExistingSnippetsPage extends BaseClass{

    public ExistingSnippetsPage(WebDriver driver){
        super(driver);
    }    

    @FindBy(how=How.ID, using="0")
	public static WebElement first_snippet_text;
}