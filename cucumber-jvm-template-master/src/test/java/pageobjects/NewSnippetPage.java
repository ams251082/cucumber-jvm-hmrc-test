package pageobjects;
import helpers.Log;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
public class NewSnippetPage extends BaseClass{

    public NewSnippetPage(WebDriver driver){
        super(driver);
    }    

    @FindBy(how=How.XPATH, using="//input[@type='submit']")
    public static WebElement submit_button;

    @FindBy(how=How.XPATH, using="//textarea[@name='snippet']")
    public static WebElement text_area;
}