package pageobjects;
import helpers.Log;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
public class GruyereSignupPage extends BaseClass{

	public GruyereSignupPage(WebDriver driver){
		super(driver);
	}    

    @FindBy(how=How.XPATH, using="//input[@type = 'text']")
    public static WebElement user_name;

    @FindBy(how=How.XPATH, using="//input[@type = 'password']")
    public static WebElement password;
	
    @FindBy(how=How.XPATH, using="//input[@type='submit']")
    public static WebElement create_account;

    @FindBy(how=How.CLASS_NAME, using="message")
	public static WebElement user_message;
}
		