Feature: Signup for a new account gruyere website and create a new snippet

Background:
    Given I open google-gruyere website
        And Gruyere: Home website is launched
        And I click signup
        And Signup for a new account page is shown


Scenario: As a user, I should be able to signup Gruyere for a new account
    When I signup for a new account
    Then "Account created" message should be displayed

 Scenario: As a user, I should be able to signup and create a new snippet
     When I signup for a new account
     Then "Account created" message should be displayed
     When I click on Home link in the header
     And I click on New Snippet link in the header
     Then New Snippet page is shown
     When I enter "this is a new snippet test" in the new snippet textbox
         And I click on Submit
     Then I should be able to see My Snippets page
         And the text under All snippets should be "this is a new snippet test"

   #This scenario should fail as this is a negative test and a probable defect
    Scenario: As a user, I should not be able to create an account using null id and password
     When I signup for a new account with blank id and password
     Then error message should be displayed
